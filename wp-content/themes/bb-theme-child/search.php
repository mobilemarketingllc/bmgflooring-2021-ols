<?php get_header(); ?>

    <div class="fl-archive container ">
        <div class="row search-row">

            <?php FLTheme::sidebar('left'); ?>

            <div class="fl-content fl-builder-content  <?php //FLTheme::content_class(); ?>" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

                <?php FLTheme::archive_page_header(); ?>

                <?php if(have_posts()) : ?>

                    <?php
                   include( ABSPATH .'/wp-content/plugins/grand-child/product-listing-templates/product-loop-search.php' );
                    ?>


                    <?php FLChildTheme::archive_nav(); ?>

                <?php else : ?>

                    <?php get_template_part('content', 'no-results'); ?>

                <?php endif; ?>

            </div>
        </div>
    </div>

<?php get_footer(); ?>